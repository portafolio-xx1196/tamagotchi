package main

import (
	"bufio"
	"fmt"
	"os"
	"tamagotchi/models"
)

func main() {
	fmt.Println("¡Bienvenido a Tamagotchi Go!")

	scanner := bufio.NewScanner(os.Stdin)

	var tamagotchiModel *models.TamagotchiModel

	// Cargar estado previo desde el archivo JSON si existe
	if _, err := os.Stat("tamagotchi.json"); err == nil {
		data, err := os.ReadFile("tamagotchi.json")
		if err != nil {
			fmt.Println("Error al cargar el estado del Tamagotchi:", err)
			os.Exit(1)
		}

		unmarshalTamagotchiModel, err := models.UnmarshalTamagotchiModel(data)
		if err != nil {
			fmt.Println("Error al cargar el estado del Tamagotchi:", err)
			os.Exit(1)
		}

		tamagotchiModel = models.NewTamagotchi(unmarshalTamagotchiModel.Name, unmarshalTamagotchiModel.Hunger, unmarshalTamagotchiModel.Happiness)
	} else {
		fmt.Print("Ingresa el nombre de tu Tamagotchi: ")
		scanner.Scan()
		name := scanner.Text()

		tamagotchiModel = models.NewTamagotchi(name, 0, 0)
	}

	tamagotchiModel.StartTimer()

	fmt.Printf("\n¡Has adoptado a %s! Cuídalo bien.\n", tamagotchiModel.Name)

	for {
		fmt.Println("\nEscribe el número correspondiente a la acción que deseas realizar:")
		fmt.Println("1. Alimentar")
		fmt.Println("2. Jugar")
		fmt.Println("3. Ver estado")
		fmt.Println("4. Salir")
		fmt.Print("\nElige una opción: ")
		scanner.Scan()
		option := scanner.Text()

		action, exists := tamagotchiModel.Actions[option]
		if exists {
			action(tamagotchiModel)
		} else {
			fmt.Println("Opción inválida. Por favor, elige una opción válida.")
		}
	}
}
