# Use a lightweight base image
FROM golang:1.20-alpine3.18 AS build

# Set environment variables
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# Set the working directory
WORKDIR /app

# Build the binary
COPY . ./
RUN go build -ldflags="-w -s" -o /xx1196-tamagotchi

# Use a minimal base image
FROM scratch
COPY --from=build /xx1196-tamagotchi /xx1196-tamagotchi

# Set the command to run the binary
ENTRYPOINT ["/xx1196-tamagotchi"]
