// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    tamagotchiModel, err := UnmarshalTamagotchiModel(bytes)
//    bytes, err = tamagotchiModel.Marshal()

package models

import (
	"encoding/json"
	"fmt"
	"os"
	"time"
)

func UnmarshalTamagotchiModel(data []byte) (TamagotchiModel, error) {
	var r TamagotchiModel
	err := json.Unmarshal(data, &r)
	return r, err
}

func (t *TamagotchiModel) Marshal() ([]byte, error) {
	return json.Marshal(t)
}

type TamagotchiModel struct {
	Name      string                            `json:"name"`
	Hunger    uint8                             `json:"hunger"`
	Happiness uint8                             `json:"happiness"`
	IsAlive   bool                              `json:"isAlive"`
	Actions   map[string]func(*TamagotchiModel) `json:"-"`
	Timer     *time.Timer                       `json:"-"`
}

//methods

func (t *TamagotchiModel) StartTimer() {
	t.Timer = time.NewTimer(10 * time.Second)
	go func() {
		<-t.Timer.C
		if t.IsAlive {
			t.IsAlive = false
			println("\nTu Tamagotchi ha muerto de inanición. ¡Intenta cuidarlo mejor la próxima vez!")
			os.Exit(0)
		}
	}()
}

func (t *TamagotchiModel) ResetTimer() {
	if t.Timer == nil {
		t.Timer = time.NewTimer(10 * time.Second)
	} else {
		if !t.Timer.Stop() {
			select {
			case <-t.Timer.C:
			default:
			}
		}
		t.Timer.Reset(10 * time.Second)
	}

	go func() {
		<-t.Timer.C
		if t.IsAlive {
			t.IsAlive = false
			fmt.Println("\nTu Tamagotchi ha muerto de inanición. ¡Intenta cuidarlo mejor la próxima vez!")
			os.Exit(0)
		}
	}()
}

func (t *TamagotchiModel) Feed() {
	t.Hunger += 10
	fmt.Printf("%s ha sido alimentado.\n", t.Name)
	t.ResetTimer()
	t.SaveState()
}

func (t *TamagotchiModel) Play() {
	t.Happiness += 10
	fmt.Printf("%s ha jugado contigo.\n", t.Name)
	t.ResetTimer()
	t.SaveState()
}

func (t *TamagotchiModel) PrintStatus() {
	fmt.Printf("\nEstado de %s:\n", t.Name)
	fmt.Printf("Hambre: %d\n", t.Hunger)
	fmt.Printf("Felicidad: %d\n", t.Happiness)
	t.ResetTimer()
}

func (t *TamagotchiModel) SaveState() {
	state, err := json.MarshalIndent(t, "", "  ")
	if err != nil {
		fmt.Println("Error al guardar el estado del Tamagotchi:", err)
		return
	}

	err = os.WriteFile("tamagotchi.json", state, 0644)
	if err != nil {
		fmt.Println("Error al guardar el estado del Tamagotchi:", err)
		return
	}

	fmt.Println("Estado del Tamagotchi guardado correctamente.")
}

func (t *TamagotchiModel) Exit() {
	fmt.Printf("\nGracias por cuidar de %s. ¡Hasta luego!\n", t.Name)
	os.Exit(0)
}

func NewTamagotchi(name string, hunger, happiness uint8) *TamagotchiModel {
	return &TamagotchiModel{
		Name:      name,
		Hunger:    hunger,
		Happiness: happiness,
		IsAlive:   true,
		Actions: map[string]func(model *TamagotchiModel){
			"1": (*TamagotchiModel).Feed,
			"2": (*TamagotchiModel).Play,
			"3": (*TamagotchiModel).PrintStatus,
			"4": (*TamagotchiModel).Exit,
		},
	}
}
