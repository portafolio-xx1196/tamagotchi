# xx1196-tamagotchi

Este es un proyecto que simula un tamagotchi

# Ejecución
Para ejecutar esta imagen, debes utilizar el siguiente comando:

```bash
docker run --rm -it -v "$(pwd)/tamagotchi.json:/tamagotchi.json" xx1196/tamagotchi:latest
```
Este comando utiliza la opción -v para montar el archivo .env en el contenedor. Asegúrate de crear un archivo .env en tu directorio actual, basado en el archivo .env.example proporcionado. También puedes modificar el comando para apuntar al directorio correcto que contiene tu archivo .env.

# Créditos
Este proyecto utiliza el modelo de lenguaje ChatGPT, entrenado por OpenAI. Puedes obtener más información sobre este modelo en https://beta.openai.com/docs/guides/chat.

# Licencia
Este proyecto está disponible bajo la Licencia MIT. Consulta el archivo LICENSE para obtener más información.